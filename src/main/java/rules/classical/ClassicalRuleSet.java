package rules.classical;

import java.util.LinkedList;
import java.util.List;

import rules.IRule;
import rules.IRuleSet;

public class ClassicalRuleSet implements IRuleSet
{

	List<IRule> rules;

	public ClassicalRuleSet()
	{
		this.rules = new LinkedList<IRule>();
		this.rules.add(new RebirthRule());
		this.rules.add(new StayAliveRule());
		this.rules.add(new UnderpopulationRule());
		this.rules.add(new OverpopulationRule());
	}

	@Override
	public List<IRule> getRules()
	{
		return new LinkedList<IRule>(this.rules);
	}

}
