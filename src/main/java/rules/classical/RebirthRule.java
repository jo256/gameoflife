package rules.classical;

import rules.NeighborRule;
import cells.ICell;
import field.IField;

/**
 * if the rule has exactly 3 living neighbors and is not alive it is reborn
 */
public class RebirthRule extends NeighborRule
{

	@Override
	protected boolean processCell(IField field, ICell cell, int livingNeighbors)
	{
		if (livingNeighbors == 3 && !cell.isAlive()) {
			cell.setAlive(true);
			return true;
		}
		return false;
	}

}
