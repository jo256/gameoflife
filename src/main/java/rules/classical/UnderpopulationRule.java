package rules.classical;

import rules.NeighborRule;
import cells.ICell;
import field.IField;

/**
 * if the rule has less than 2 living neighbors it dies because of underpopulation
 */
public class UnderpopulationRule extends NeighborRule
{

	@Override
	protected boolean processCell(IField field, ICell cell, int livingNeighbors)
	{
		if (livingNeighbors < 2) {
			cell.setAlive(false);
			return true;
		}
		return false;
	}

}
