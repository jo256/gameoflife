package rules.classical;

import rules.NeighborRule;
import cells.ICell;
import field.IField;

/**
 * if the rule has more than 3 living neighbors it dies because of overpopulation
 */
public class OverpopulationRule extends NeighborRule
{

	@Override
	protected boolean processCell(IField field, ICell cell, int livingNeighbors)
	{
		if (livingNeighbors > 3) {
			cell.setAlive(false);
			return true;
		}
		return false;
	}

}
