package rules.classical;

import rules.NeighborRule;
import cells.ICell;
import field.IField;

/**
 * if the rule has 2 or 3 living neighbors it stays alive
 */
public class StayAliveRule extends NeighborRule
{

	@Override
	protected boolean processCell(IField field, ICell cell, int livingNeighbors)
	{
		if (cell.isAlive() && (livingNeighbors == 2 || livingNeighbors == 3))
			return true;
		return false;
	}

}
