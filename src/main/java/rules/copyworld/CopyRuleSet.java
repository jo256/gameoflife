package rules.copyworld;

import java.util.LinkedList;
import java.util.List;

import rules.IRule;
import rules.IRuleSet;

public class CopyRuleSet implements IRuleSet
{
	List<IRule> rules;

	public CopyRuleSet()
	{
		this.rules = new LinkedList<IRule>();
		this.rules.add(new DeathRule());
		this.rules.add(new LifeRule());
	}

	@Override
	public List<IRule> getRules()
	{
		return new LinkedList<IRule>(this.rules);
	}

}
