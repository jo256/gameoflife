package rules.copyworld;

import rules.NeighborRule;
import cells.ICell;
import field.IField;

public class LifeRule extends NeighborRule
{

	@Override
	protected boolean processCell(IField field, ICell cell, int livingNeighbors)
	{
		if (livingNeighbors == 1 || livingNeighbors == 3 || livingNeighbors == 5
				|| livingNeighbors == 7) {
			cell.setAlive(true);
			return true;
		}

		return false;
	}

}
