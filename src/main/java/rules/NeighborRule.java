package rules;

import cells.ICell;
import field.IField;

/**
 * This abstract class is used to simplify the implementation of a classical rule by providing the
 * number of living neighbors to its subclasses.
 */
public abstract class NeighborRule implements IRule
{

	@Override
	public boolean applyRule(IField field, ICell cell)
	{
		int livingNeighbors = 0;
		int x = cell.getX();
		int y = cell.getY();

		// COUNT LIVING NEIGHBORS

		// west neighbor
		livingNeighbors += this.checkCell(x - 1, y, field);
		// east neighbor
		livingNeighbors += this.checkCell(x + 1, y, field);
		// north neighbor
		livingNeighbors += this.checkCell(x, y - 1, field);
		// south neighbor
		livingNeighbors += this.checkCell(x, y + 1, field);
		// north west neighbor
		livingNeighbors += this.checkCell(x - 1, y - 1, field);
		// north east neighbor
		livingNeighbors += this.checkCell(x + 1, y - 1, field);
		// south west neighbor
		livingNeighbors += this.checkCell(x - 1, y + 1, field);
		// south east neighbor
		livingNeighbors += this.checkCell(x + 1, y + 1, field);

		return this.processCell(field, cell, livingNeighbors);
	}

	private int checkCell(int x, int y, IField field)
	{
		if (field.getCell(x, y).isAlive())
			return 1;
		return 0;
	}

	/**
	 * Overridden by sublasses to implement the actual rule behavior
	 * 
	 * @param field the field where the cell lives in
	 * @param cell the cell on which rule should be applied
	 * @param livingNeighbors the number of living neighbors the cell has
	 * @return true if the requirements for the rule were met and the rule was applied, false
	 *         otherwise
	 */
	protected abstract boolean processCell(IField field, ICell cell, int livingNeighbors);

}
