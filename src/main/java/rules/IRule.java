package rules;

import cells.ICell;
import field.IField;

public interface IRule
{
	/**
	 * Applies the rule to the given cell in the supplied field.
	 * 
	 * @param field the field where the cell lives in
	 * @param cell the cell on which the rule should be applied
	 * @return true if the requirements for the rule were met and the rule was applied, false
	 *         otherwise
	 */
	public boolean applyRule(IField field, ICell cell);
}
