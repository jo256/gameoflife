package rules;

import java.util.List;

public interface IRuleSet
{
	List<IRule> getRules();
}
