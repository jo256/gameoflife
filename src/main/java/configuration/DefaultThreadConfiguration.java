package configuration;

import rules.IRuleSet;

public class DefaultThreadConfiguration implements IThreadConfiguration
{
	private IRuleSet ruleSet;
	private boolean running;
	private long duration;
	private boolean resetPending;

	public DefaultThreadConfiguration(IRuleSet rules, boolean running)
	{
		super();
		this.ruleSet = rules;
		this.running = running;
	}

	@Override
	public boolean isRunning()
	{
		return this.running;
	}

	@Override
	public void setRunning(boolean running)
	{
		this.running = running;
	}

	@Override
	public IRuleSet getRuleSet()
	{
		return this.ruleSet;
	}

	@Override
	public long getDuration()
	{
		return this.duration;
	}

	@Override
	public void setDuration(long duration)
	{
		this.duration = duration;
	}

	@Override
	public boolean getAndForgetResetPending()
	{
		boolean resetPending = this.resetPending;
		this.resetPending = false;
		return resetPending;
	}

	@Override
	public void setResetPending(boolean resetPending)
	{
		this.resetPending = resetPending;
	}

}
