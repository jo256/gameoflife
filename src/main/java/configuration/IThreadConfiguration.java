package configuration;

import rules.IRuleSet;

public interface IThreadConfiguration
{
	/**
	 * Returns a ruleset containing the rules applied to the cells
	 * 
	 * @return a ruleset containing the rules applied to the cells
	 */
	public IRuleSet getRuleSet();

	/**
	 * Returns whether the rules should be processed
	 * 
	 * @return true if the thread should process the rules, false otherwise
	 */
	public boolean isRunning();

	/**
	 * Configures whether the rules should be processed
	 * 
	 * @param running indicates whether the rules should be processed
	 */
	public void setRunning(boolean running);

	/**
	 * Returns the duration of a cell generation
	 * 
	 * @return the duration of a cell generation
	 */
	public long getDuration();

	/**
	 * Sets the duration of a cell generation
	 * 
	 * @param duration the duration of a cell generation
	 */
	public void setDuration(long duration);

	/**
	 * Returns and resets the reset pending status
	 * 
	 * @return true if a reset is pending, false otherwise
	 */
	public boolean getAndForgetResetPending();

	/**
	 * Sets whether a reset is pending
	 * 
	 * @param resetPending indicates whether a reset is pending
	 */
	public void setResetPending(boolean resetPending);
}
