package gui;

import javax.swing.JComponent;

import cells.ICell;

/**
 * A graphical cell which can be displayed in a swing panel
 */
public interface ISwingCell extends ICell
{
	/**
	 * Returns the swing component representing the cell
	 * 
	 * @return the swing component representing the cell
	 */
	public JComponent getComponent();
}
