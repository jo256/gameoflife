package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import cells.DeadCellFactory;
import cells.LivingCellFactory;
import configuration.IFieldConfiguration;
import configuration.IThreadConfiguration;

public class ConfigPanel extends JPanel
{
	private JToggleButton startStopButton;
	private JCheckBox borderLivingCheckBox;
	private JSpinner durationSpinner;
	private JButton resetButton;
	private final IThreadConfiguration threadConfiguration;
	private final IFieldConfiguration fieldConfiguration;

	public ConfigPanel(IThreadConfiguration threadConfiguration,
			IFieldConfiguration fieldConfiguration)
	{
		this.threadConfiguration = threadConfiguration;
		this.fieldConfiguration = fieldConfiguration;
		this.startStopButton = new JToggleButton("Running");
		this.borderLivingCheckBox = new JCheckBox("Border alive");
		this.durationSpinner = new JSpinner(new SpinnerNumberModel(
				threadConfiguration.getDuration(), 10, 10000, 10));
		this.resetButton = new JButton("Reset");
		this.add(this.startStopButton);
		this.add(this.borderLivingCheckBox);
		this.add(new JLabel("Duration:"));
		this.add(this.durationSpinner);
		this.add(this.resetButton);

		this.startStopButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				ConfigPanel.this.threadConfiguration
						.setRunning(!ConfigPanel.this.threadConfiguration.isRunning());
				// ConfigPanel.this.borderLivingCheckBox.setEnabled(!ConfigPanel.this.configuration.isRunning());
			}
		});

		this.borderLivingCheckBox.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (ConfigPanel.this.borderLivingCheckBox.isSelected()) {
					ConfigPanel.this.fieldConfiguration
							.setOuterCellFactory(new LivingCellFactory());
				}
				else {
					ConfigPanel.this.fieldConfiguration.setOuterCellFactory(new DeadCellFactory());
				}
			}
		});

		this.resetButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				ConfigPanel.this.threadConfiguration.setResetPending(true);
			}
		});

		this.durationSpinner.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent e)
			{
				ConfigPanel.this.threadConfiguration
						.setDuration(((Double) ConfigPanel.this.durationSpinner.getModel()
								.getValue()).longValue());
			}
		});
	}
}
