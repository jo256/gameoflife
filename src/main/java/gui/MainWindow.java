package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import configuration.IFieldConfiguration;
import configuration.IThreadConfiguration;

public class MainWindow extends JFrame
{
	public MainWindow(String title, ISwingField field, IThreadConfiguration threadConfiguraiton,
			IFieldConfiguration fieldConfiguration)
	{
		super(title);
		this.setSize(600, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.setLayout(new BorderLayout());
		this.add(field.getComponent(), BorderLayout.CENTER);
		this.add(new ConfigPanel(threadConfiguraiton, fieldConfiguration), BorderLayout.SOUTH);

		this.setVisible(true);
	}
}
