package gui;

import java.awt.GridLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;

import cells.ICell;
import field.IField;

public class SwingField implements ISwingField
{
	private JPanel panel;
	private IField innerField;
	private ISwingCellFactory cellFactory;
	private ISwingCell[][] cells;

	public SwingField(IField innerField, ISwingCellFactory cellFactory)
	{
		this.innerField = innerField;
		this.cellFactory = cellFactory;
		this.cells = new ISwingCell[this.innerField.getWidth()][this.innerField.getHeight()];
		this.panel = new JPanel();
		this.panel.setLayout(new GridLayout(this.getWidth(), this.getHeight()));
		this.initCells();
	}

	protected void initCells()
	{
		for (int y = 0; y < this.getHeight(); y++) {
			for (int x = 0; x < this.getWidth(); x++) {
				ISwingCell swingCell = this.cellFactory.createSwingcellFromCell(this.innerField
						.getCell(x, y));
				this.cells[y][x] = swingCell;
				this.panel.add(swingCell.getComponent(), y * this.getWidth() + x);
			}
		}
	}

	@Override
	public ICell getCell(int x, int y)
	{
		// if the coordinates are out of bound let the field decide
		if (x < 0 || x > this.innerField.getWidth() - 1 || y < 0
				|| y > this.innerField.getHeight() - 1)
			return this.innerField.getCell(x, y);

		return this.cells[y][x];
	}

	@Override
	public int getWidth()
	{
		return this.innerField.getWidth();
	}

	@Override
	public int getHeight()
	{
		return this.innerField.getHeight();
	}

	@Override
	public JComponent getComponent()
	{
		return this.panel;
	}

}
