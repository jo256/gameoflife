package gui;

import cells.ICell;

public interface ISwingCellFactory
{
	public ISwingCell createSwingcellFromCell(ICell cell);
}
