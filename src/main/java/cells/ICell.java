package cells;

public interface ICell
{
	/**
	 * Stores a new pending alive status. Keep in mind that commitAlive must be called to make this
	 * state take effect
	 * 
	 * @param alive the new alive status
	 */
	public void setAlive(boolean alive);

	/**
	 * Makes the alive status previously set via setAlive take effect
	 */
	public void commitAlive();

	/**
	 * Returns whether the cell is alive
	 * 
	 * @return true if the cell is alive, false otherwise
	 */
	public boolean isAlive();

	/**
	 * Returns the x coordinate of the cell
	 * 
	 * @return the x coordinate of the cell
	 */
	public int getX();

	/**
	 * Returns the y coordinate of the cell
	 * 
	 * @return the y coordinate of the cell
	 */
	public int getY();
}
