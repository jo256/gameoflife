package compositionroot;

import rules.IRule;
import cells.ICell;
import configuration.IThreadConfiguration;
import field.IField;

public class WorkerThread extends Thread
{
	private IField field;
	private IThreadConfiguration configuration;

	public WorkerThread(IField field, IThreadConfiguration configuration)
	{
		super();
		this.field = field;
		this.configuration = configuration;
	}

	@Override
	public void run()
	{
		while (true) {
			if (this.configuration.getAndForgetResetPending()) {
				this.performActionOnField(this.clearAction);
			}
			if (this.configuration.isRunning()) {
				this.performActionOnField(this.applyRuleAction);
				this.performActionOnField(this.commitAction);
			}
			try {
				Thread.sleep(this.configuration.getDuration());
			}
			catch (InterruptedException exc) {
				break;
			}
		}
	}

	/**
	 * Performs the supplied action on every cell on the field
	 * 
	 * @param action the action which should be performed on the cells
	 */
	private void performActionOnField(IAction action)
	{
		for (int x = 0; x < this.field.getWidth(); x++) {
			for (int y = 0; y < this.field.getHeight(); y++) {
				action.perform(this.field.getCell(x, y));
			}
		}
	}

	/**
	 * Specifies an action which can be performed on a cell
	 */
	private interface IAction
	{
		/**
		 * Performs some sort of action on the supplied cell
		 * 
		 * @param cell the cell on which the action should be performed
		 */
		public void perform(ICell cell);
	}

	/**
	 * Applies all rules on the supplied cell
	 */
	private IAction applyRuleAction = new IAction()
	{
		@Override
		public void perform(ICell cell)
		{
			for (IRule rule : WorkerThread.this.configuration.getRuleSet().getRules()) {
				if (rule.applyRule(WorkerThread.this.field, cell)) {
					break;
				}
			}
		}
	};

	/**
	 * Kills the supplied cell
	 */
	private IAction clearAction = new IAction()
	{
		@Override
		public void perform(ICell cell)
		{
			cell.setAlive(false);
			cell.commitAlive();
		}

	};

	/**
	 * Commits the alive state of the cell
	 */
	private IAction commitAction = new IAction()
	{
		@Override
		public void perform(ICell cell)
		{
			cell.commitAlive();
		}
	};

}
