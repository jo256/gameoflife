package compositionroot;

import rules.IRuleSet;
import rules.classical.ClassicalRuleSet;
import rules.copyworld.CopyRuleSet;
import cells.DeadCellFactory;
import cells.ICellFactory;
import cells.LivingCellFactory;
import configuration.DefaultFieldConfiguration;
import configuration.DefaultThreadConfiguration;
import configuration.IFieldConfiguration;
import configuration.IThreadConfiguration;
import field.Field;
import field.IField;
import gui.ISwingCellFactory;
import gui.ISwingField;
import gui.MainWindow;
import gui.SwingCellFactory;
import gui.SwingField;

public class Main
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		IRuleSet classical = new ClassicalRuleSet();
		IRuleSet copy = new CopyRuleSet();

		IThreadConfiguration threadConfiguration = new DefaultThreadConfiguration(classical, false);
		threadConfiguration.setDuration(250l);
		ICellFactory inner = new LivingCellFactory();
		ICellFactory outer = new DeadCellFactory();
		ICellFactory initial = new DeadCellFactory();
		IFieldConfiguration fieldConfiguration = new DefaultFieldConfiguration(inner, outer,
				initial);

		IField field = new Field(3, 3, fieldConfiguration);
		ISwingCellFactory swingCellFactory = new SwingCellFactory();
		ISwingField swingField = new SwingField(field, swingCellFactory);
		WorkerThread thread = new WorkerThread(swingField, threadConfiguration);
		new MainWindow("Game of Life", swingField, threadConfiguration, fieldConfiguration);
		thread.start();
	}

}
