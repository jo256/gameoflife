package cells;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Test;

public class CellTest{

	@Test
	public void testCellConstructor()
	{
		int x = 10;
		int y = 20;
		
		Cell cell = new Cell(x, y);
		
		assertEquals(x, cell.getX());
		assertEquals(y, cell.getY());
		assertFalse(cell.isAlive());
	}
	
	public void testCellCommitAlive()
	{
		Cell cell = new Cell(10, 20);
		
		cell.setAlive(true);
		assertFalse(cell.isAlive());
		
		cell.commitAlive();
		assertTrue(cell.isAlive());
		
		cell.setAlive(false);
		assertTrue(cell.isAlive());
		
		cell.commitAlive();
		assertFalse(cell.isAlive());
		
	}
}
