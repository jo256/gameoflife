package cells;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class LivingCellFactoryTest
{
	@Test
	public void testCreateCell()
	{
		LivingCellFactory factory = new LivingCellFactory();
		int x = 10;
		int y = 20;
		ICell cell = factory.createCell(x, y);
		
		assertEquals(x, cell.getX());
		assertEquals(y, cell.getY());
		assertTrue(cell.isAlive());
	}
}
