package field;

import static org.mockito.Mockito.mock;

import org.junit.Test;

import configuration.IFieldConfiguration;

public class FieldTest
{
	@Test(expected=IllegalArgumentException.class)
	public void testFieldCreationWithNegativeWidth()
	{
		IField field = new Field(-1, 10, mock(IFieldConfiguration.class));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFieldCreationWithZeroeHeight()
	{
		IField field = new Field(10, 0, mock(IFieldConfiguration.class));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFieldCreationWithZeroWidth()
	{
		IField field = new Field(0, 10, mock(IFieldConfiguration.class));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFieldCreationWithNegativeHeight()
	{
		IField field = new Field(10, -1, mock(IFieldConfiguration.class));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFieldCreationWithNullConfiguration()
	{
		IField field = new Field(10, 10, null);
	}
	

}
